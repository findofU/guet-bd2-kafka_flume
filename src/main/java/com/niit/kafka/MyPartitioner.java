package com.niit.kafka;

import org.apache.kafka.clients.producer.Partitioner;
import org.apache.kafka.common.Cluster;

import java.util.Map;

/*
* 笔记上是采取随机数进行自定义分区策略
*
* 这里我们做Key值 进行自定分区策略
* */
public class MyPartitioner  implements Partitioner {


    @Override
    public int partition(String s, Object key, byte[] keyBytes, Object value, byte[] valueBytes, Cluster cluster) {

        String msgKey = key.toString();
        int partition;
        if(msgKey.contains("BD2_")){
            partition = 0;
        }else if(msgKey.contains("guidian")){
            partition = 1;
        }else {
            partition = 2;
        }

        return partition;// 0 -->分区号
    }

    @Override
    public void close() {

    }

    @Override
    public void configure(Map<String, ?> map) {

    }
}
